﻿<!DOCTYPE html>
<?php  session_start(); ?>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Аренда домов</title>
    <link rel="stylesheet" href="css/bootstrap.css" />
    <link rel="stylesheet" href="css/site.css" />
	
    
	<script src="scripts/jquery-latest.js"></script>
	<script src="admin/js/jquery.wallform.js"></script>
	<script src="scripts/myjquery.js" type="text/javascript"></script>
	<script type="text/javascript" src="scripts/bootstrap.min.js"></script>	
	<script>
 		$(document).ready(function() { 
		
            $('#find_Form').submit(function(event)			{ 
			    $("#div_info").html('');
				
				event.preventDefault();
				var type_building = document.getElementById('type_building').value;
				var year_built1 = document.getElementById('year_built1').value;
				var year_built2 = document.getElementById('year_built2').value;
				var level1 = document.getElementById('level1').value;
				var level2 = document.getElementById('level2').value;
				var area_of_land1 = document.getElementById('area_of_land1').value;
				var area_of_land2 = document.getElementById('area_of_land2').value;
				var heating = document.getElementById('heating').value;
				var canal = document.getElementById('canal').value;
				var area1 = document.getElementById('area1').value;
				var area2 = document.getElementById('area2').value;
				var livingroom_area1 = document.getElementById('livingroom_area1').value;
				var livingroom_area2 = document.getElementById('livingroom_area2').value;
				var kitchen1 = document.getElementById('kitchen1').value;
				var kitchen2 = document.getElementById('kitchen2').value;
				var sortirovka = document.getElementById('sortirovka').value;
				var view_amount = document.getElementById('view_amount').value;
				
					$(".btn-group label").removeClass("active");	
					$("#dollarl").addClass("active");							
					currency='dollar';
				
				var data = $('#find_Form').serializeArray();
					data.push({name: 'type_building', value: type_building});
					data.push({name: 'year_built1', value: year_built1});
					data.push({name: 'year_built2', value: year_built2});
					data.push({name: 'level1', value: level1});
					data.push({name: 'level2', value: level2});
					data.push({name: 'area_of_land1', value: area_of_land1});
					data.push({name: 'area_of_land2', value: area_of_land2});
					data.push({name: 'heating', value: heating});
					data.push({name: 'canal', value: canal});
					data.push({name: 'area1', value: area1});
					data.push({name: 'area2', value: area2});
					data.push({name: 'livingroom_area1', value: livingroom_area1});
					data.push({name: 'livingroom_area2', value: livingroom_area2});
					data.push({name: 'kitchen1', value: kitchen1});
					data.push({name: 'kitchen2', value: kitchen2});
					data.push({name: 'sortirovka', value: sortirovka});
					data.push({name: 'view_amount', value: view_amount});
					data.push({name: 'hot_offers', value: hot_offers});
				
								$.ajax({
									url: "find_rent_cottage.php",
									type: "post",
									data: data,
									beforeSend:function(){ 
										 $("#loadImg").show();
										 $("#div_info").hide();
									 }, 
									success: function(data){
										$("#loadImg").hide();
										$("#div_info").html(data);
										$("#div_info").show();
									},
									error:function(){
										$("#loadImg").hide();
										$("#div_info").show();
										}
								});
		
			});
       }); 
	</script>
	
	<script type="text/javascript">
		   var currency = 'dollar';
		   jQuery(function ($) {
		   		var money = document.getElementsByClassName("price_value");	
				var money1 = document.getElementsByClassName("price");	
				
				$(document).on('change', 'input:radio[id="dollar"]', function (event) {
					if(currency=='euro'){
						for(var i=0; i<money.length; i++){
							money[i].value = Math.round(money[i].value*1.352065);
							money1[i].innerHTML = money[i].value + " $";
						}
					}
					if(currency=='tenge'){
						for(var i=0; i<money.length; i++){
							money[i].value = Math.round(money[i].value/183.519912);
							money1[i].innerHTML = money[i].value + " $";
						}
					}
				
					currency = 'dollar';
					
				});
				
				$(document).on('change', 'input:radio[id="euro"]', function (event) {
					if(currency=='dollar'){
						for(var i=0; i<money.length; i++){
							money[i].value = Math.round(money[i].value/1.352065);
							money1[i].innerHTML = money[i].value + " &#8364";
						}
					}
					if(currency=='tenge'){
						for(var i=0; i<money.length; i++){
							money[i].value = Math.round(money[i].value/248.13085);
							money1[i].innerHTML = money[i].value + " &#8364";
						}
					}
					
					currency = 'euro';
				});
				
				$(document).on('change', 'input:radio[id="tenge"]', function (event) {
					if(currency=='dollar'){
						for(var i=0; i<money.length; i++){
							money[i].value = Math.round(money[i].value*183.519912);
							money1[i].innerHTML = money[i].value + " &#8376";
						}
					}
					if(currency=='euro'){
						for(var i=0; i<money.length; i++){
							money[i].value = Math.round(money[i].value*248.13085);
							money1[i].innerHTML = money[i].value + " &#8376";
						}
					}
					
					currency = 'tenge'; 
				});
			});
  	</script>
	
	<script>
 
		jQuery(function ($) {
		 
			$(document).on("change","select[id='sortirovka']", function(){
			 
				$('#find_Form').submit();
			 
			});
		 
		});
	 
	</script>
	<script>
 
		jQuery(function ($) {
		 
			$(document).on("change","select[id='view_amount']", function(){
			 
				$('#find_Form').submit();
			 
			});
		 
		});
	 
	</script>
	

</head>
<body>
  <?php include("blocks/navbar.php"); ?>
		
  <div class="container body-content">
            <div class="row">
				<div class="col-md-12">
					<h1 style="float:left" style="font-family:Verdana, Arial, Helvetica, sans-serif:">Аренда домов</h1>
					
					<div ></div>
				</div>
			</div>
			<hr />
			
			<div class="search" style="height:100px; margin-bottom:0px;">
			<div class="row" style="padding-top: 16px; padding-left:20px;">
				<form id="find_Form"  method="post">
						<div class="col-md-2" style="width:117px; padding-right:0px;">
								<label class="control-label input-sm" style="padding-right:0px;">Сколько комнат</label>
						</div>
						<div class="col-md-2">
							<div class="input-group">
							<select style="width:97px;" class="form-control input-sm" name="from_rooms">
								<option>неважно</option>
							<?php 
								for($i=1; $i<16; $i++){
								echo "<option>".$i."</option>"; }
								?>
							</select>
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<select style="width:97px;" class="form-control input-sm" name="to_rooms">
								<option>неважно</option>
							<?php 
								for($i=1; $i<16; $i++){
									echo "<option>".$i."</option>"; }
								?>
							</select>
							</div>
							<div class="input-group" style="margin-top:5px;">
								<select  class="form-control input-sm" style="border-radius:3px;" name="how_rent">
									<option>неважно</option>
									<option>по часам</option>
									<option>посуточно</option>
									<option>помесячно</option>
									<option>поквартально</option>	
								</select>
							</div>	
						</div>	
							<div class="col-md-2" style="width:50px; padding-right:0px; margin-left:50px;">
								<label class="control-label input-sm" style="padding-right:0px;">Цена</label>
						</div>
						<div class="col-md-2" style="padding-right:0px;">
								<input class="form-control input-sm" type="text" placeholder="Цена от" name="from_price"
														style="border-bottom-right-radius: 0px;
															   border-top-right-radius: 0px;"> 
						<div style="margin-top:5px;"><input class="form-control input-sm" type="text" placeholder="Цена до" name="to_price"></div>
						</div>	
						<div class="col-md-1" style="width:70px; padding-left:0px; " >
								<select class="form-control input-sm" 
															style="background: #EEEEEE; font-weight:bold;
																   border-top-left-radius: 0px;
															  	   border-bottom-left-radius: 0px;
																   border-left:0px;
																   background-repeat:repeat-x; background:url(images/search.png);
																   "
																    >
									<option style="background:#FFFFFF;">&#8364 </option>
									<option style="background:#FFFFFF;">$ </option>
									<option style="background:#FFFFFF;">&#8376 </option>
								</select>
						</div>
					<div class="col-md-1">
  							<label class="checkbox-inline">
  							<input type="checkbox" value="1" name="photo" > с фото
							</label>
					</div>
							
					<div class="col-md-1">
						<button class="btn btn-primary" type="submit" >найти</button>
					</div>		
				</form>
			</div>
			</div>
			
			<form id="advanced_form">
			<div id="advanced_search">
						<div class="row" style="padding-top:20px;  margin-left:20px;">
						<div class="col-md-2" style="width:125px; padding-right:0px;">
								<label class="control-label input-sm" style="padding-right:0px;">Тип дома</label>
								<label class="control-label input-sm" style="padding-right:0px; margin-top:5px;">Год постройки</label>
						</div>
						<div class="col-md-3" style="width: 219px; padding-left:0px;" >
							<select class="form-control input-sm" id="type_building">
									<option>любой </option>		
									<option>кирпичный </option>
									<option>панельный</option>
									<option>монолитный </option>
									<option>капксно-камышитовый </option>
									<option>иное</option>
								</select>
								
							<div class="input-group" style="margin-top:10px;">
							<select style="width:85px;" class="form-control input-sm" id="year_built1" >
								<option>любой </option>	
							<?php 
								for($i=1980; $i<=2014; $i++){
								echo "<option>".$i."</option>"; }
								?>
							</select>
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<select style="width:85px;" class="form-control input-sm" id="year_built2" >
								<option>любой </option>
							<?php 
								for($i=1980; $i<=2014; $i++){
									echo "<option>".$i."</option>"; }
								?>
							</select>
							</div>
						</div>		
						<div class="col-md-3" style="width: 170px;  margin-left:20px;">
							<label class="control-label input-sm" style="padding-right:0px;">Количество уровней с</label>
							<label class="control-label input-sm" style="padding-right:0px; margin-top:5px;">Площадь участка от</label>
						</div>
						<div class="col-md-2" style="padding-left:0px;">
							<div class="input-group">
							<select style="width:85px;" class="form-control input-sm" id="level1" >
								<option>любой </option>	
							<?php 
								for($i=1; $i<=5; $i++){
								echo "<option>".$i."</option>"; }
								?>
							</select>
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<select style="width:85px;" class="form-control input-sm" id="level2" >
								<option>любой </option>	
							<?php 
								for($i=1; $i<=5; $i++){
									echo "<option>".$i."</option>"; }
								?>
							</select>
							</div>
							<div class="input-group" style="margin-top:10px;">
							<select style="width:85px;" class="form-control input-sm" id="area_of_land1" >
								<option>любой </option>	
							<?php 
								for($i=1; $i<=100; $i++){
								echo "<option>".$i."</option>"; }
								?>
							</select>
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<select style="width:85px;" class="form-control input-sm" id="area_of_land2" >
								<option>любой </option>	
							<?php 
								for($i=1; $i<=100; $i++){
									echo "<option>".$i."</option>"; }
								?>
							</select>
							</div>
							</div>
							<div class="col-md-1" style="margin-left: 50px;">
								<label class="control-label input-sm" style="padding-right:0px;">Отопление</label>
								<label class="control-label input-sm" style="padding-right:0px; margin-top:5px;">Канализация</label>
							</div>
							<div class="col-md-3" style="width: 210px;">
								<select class="form-control input-sm" id="heating">		
										<option>не важно</option>
										<option>центральное</option>
										<option>на газе</option>
										<option>на твердом топливе</option>
										<option>на жидком топливе</option>
										<option>смешанное</option>
										<option>без отопления</option>
								</select>
									
								<select class="form-control input-sm" style="margin-top: 10px;" id="canal" >
										<option>не важно</option>
										<option>центральная</option>
										<option>есть возможность подведения</option>
										<option>септик</option>
										<option>нет</option>
								</select>		
							</div>	
						</div>
						<hr>
						<div class="row" >
						<div class="col-md-1" align="right" style="margin-left:50px;">
							<label class="control-label input-sm" style="padding-right:0px;">Общая</label>
						</div>	
						<div class="col-md-2">	
							<div class="input-group">
							<input type="text" class="form-control input-sm" id='area1'>
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<input type="text" class="form-control input-sm" id="area2">
							</div>	
						</div>	
						<div class="col-md-1" align="right" style="margin-left:50px;">
							<label class="control-label input-sm" style="padding-right:0px;">Жилая</label>
						</div>	
						<div class="col-md-2">	
							<div class="input-group">
							<input type="text" class="form-control input-sm" id="livingroom_area1">
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<input type="text" class="form-control input-sm" id="livingroom_area2">
							</div>	
						</div>	
						<div class="col-md-1" align="right" style="margin-left:50px;">
							<label class="control-label input-sm" style="padding-right:0px;">Кухня</label>
						</div>	
						<div class="col-md-2">	
							<div class="input-group">
							<input type="text" class="form-control input-sm" id="kitchen1" >
						<span class="input-group-addon input-sm" style="border-right:0px; border-left:0px; background-repeat:repeat-x; background:url(images/search.png);"> до </span>
							<input type="text" class="form-control input-sm" id="kitchen2" >
							</div>	
						</div>	
						</div>
			</div>
			
			</form>
			<div style="float:left; width:75%;">
			<div class="settings">
				<div class="row" style="padding-top:5px;">
					<div class="col-md-4">
						<div class="form-group">
							<label class="control-label col-xs-4 input-sm" style="padding-left:15px;">Сортировка</label>
							<div class="col-xs-1" style="padding-left:5px;">
								<select style="width:143px; height:28px;" class="form-control input-sm" id="sortirovka" >
									<option>Самые новые</option>
									<option>Самые дешевые</option>
									<option>Самые дорогие</option>
								</select>
							</div>
						</div>
					</div>	
					<form id="currency" >
					<div class="col-md-3">
						<div class="form-group">
							<label class="control-label col-xs-3 input-sm" style="margin-right:15px;" >Валюта</label>
							<div class="btn-group" data-toggle="buttons" data  >
								<label class="btn btn-default" style="color:#3399CC; height:28px; padding-top:3px;">
									<input type="radio" name="options" id="euro" > &#8364 
								</label>
								<label class="btn btn-default active" style="color:#3399CC; height:28px; padding-top:3px;" id="dollarl">
									<input type="radio" name="options" id="dollar" checked="checked"> $
								</label>
								<label class="btn btn-default" style="color:#3399CC; height:28px; padding-top:3px;">
									<input type="radio" name="options" id="tenge" > &#8376
								</label>
							</div>	
						</div>
					</div>
					</form>
					<div class="col-md-3" style="width:190px;">
  							<label class="checkbox-inline" style="padding-top:2px;" id="hot_label">
  							<input type="checkbox" value="1"  style="display:none;" id="hot_offers" >
							</label>
					</div>	
					<div class="col-md-2">
						<div class="form-group">
							<label class="control-label col-xs-5 input-sm" style="margin-right:8px;">Показать</label>
							<div class="col-xs-1">
								<select style="width:67px; height:28px;" class="form-control input-sm" id="view_amount">
									<option>все</option>
								    <?php 
										for($i=5; $i<26; $i=$i+5){
											echo "<option>".$i."</option>";
										}
										?>
								</select>
							</div>
						</div>
					</div>
				</div>	
			</div>	
			
			
			<div id="loadImg"  style="display:none; height:150px; width:60%;" align="right">
					<img  src="images/progressbar.gif"  style="margin-top:30px;"/>
			</div>
			<div id="div_info">
			
			<?php 
			
			include("db.php");
			$query = mysql_query('select * from cottage_rent order by id desc');
			
			
				
			
			
			while($result=mysql_fetch_array($query)){ 
				$photo = mysql_query("select path from image_cottage_rent where cottage_rent_id='".$result['id']." limit 1'");
				
				$photo_path = mysql_fetch_array($photo);
				
				
				
				
			echo	"<table class='info' >
					<tr>
						
						<td class='tdImgList' ><div class='divList_selling' style='margin:10px;'>";
						
						if(mysql_num_rows($photo)==0){
							echo "<img  class='imgList_selling' src='images/noimage.png' style='width:100px; margin-left:12px; margin-right:12px;'/>";
						}
						else{
							echo "<img  class='imgList_selling' src='uploads/image/".$photo_path['path']."' />";
						}
						
			
			echo "</div>
						</td>
						<td align='left' valign='top' style='position:relative;'> 
							<h4 class='name'>
							<a href='view_rent_cottage.php?id=".$result['id']."'>
								".$result['rooms']." -комнатная, 
								".$result['main_street']." ".$result['home_number'];
								
								if($result['second_street']!=''){
									echo " - ".$result['second_street'];
								}								
								
						echo 	"</a></h4>
							<div class='advanced_info'>
								<div>".$result['cottage_town']."</div>
								<div>Площадь - ".$result['area'].", уровней ".$result['level'].", ".$result['how_rent']."</div>
							</div>
							<div class='viewed'>Просмотров 145</div>
						</td>
						<td class='price' align='right'>".$result['price']." $</td>
					</tr>
				</table><input class='price_value' type='hidden' value='".$result['price']."' >";
				
				}
				?>
				</div>
				
				<div style=" height:100px; margin-top:0px; border:1px solid  #E4E4E4;" align="center">
					<ul class="pagination  pagination-sm">
					  <li><a href="#">&laquo;</a></li>
					  <li class="active"><a href="#">1</a></li>
					  <li><a href="#">2</a></li>
					  <li><a href="#">3</a></li>
					  <li><a href="#">4</a></li>
					  <li><a href="#">5</a></li>
					  <li><a href="#">&raquo;</a></li>
					</ul>
				</div>
				</div>
				<div style="margin-left:875px; margin-top:0px;" align="center">
					<h3 align="center" style="margin-top:18px; margin-bottom:25px;">Коттеджные городки</h3>
					<table class="main_ads" style="border:1px solid #F0F0F0; background:#F9F9F9; border-radius:3px;">
					<tr>
						<td valign='top' > 
							<div style='border: 1px solid #DEDEDE; padding:5px; background:#FFFFFF; border-radius: 3px;'>
								<div style='border: 1px solid #DEDEDE; padding:3px; border-radius:2px;'>
									<img src='images/ZK/triumf.jpg' style='width:150px; height:100px;'/>
								</div>
							<small style='font-family:Verdana, Arial, Helvetica, sans-serif; color:#317EAC;'>Академия<br />
							<span style="color:#3B3B3B">Есильский район</br>
							п. Косщи, ул. Республик</br></span>
							<strong style='color:#F0C800;'>Рейтинг 9.25/10 <i class="glyphicon glyphicon-star"></i></strong></small>
								</div>	
							</td>		
					</tr>
					<tr>
						<td valign='top' > 
							<div style='border: 1px solid #DEDEDE; padding:5px; background:#FFFFFF; border-radius: 3px; max-width: 170px;'>
								<div style='border: 1px solid #DEDEDE; padding:3px; border-radius:2px;'>
									<img src='images/ZK/AltynTulip.jpg' style='width:150px; height:100px;'/>
								</div>
							<small style='font-family:Verdana, Arial, Helvetica, sans-serif; color:#317EAC;'>Триумфиальный<br />
							<span style="color:#3B3B3B">Алматинский район</br>
							Туран — Ханов Керея и Жанибека, 2</br></span>
							<strong style='color:#F0C800;'>Рейтинг 9.65/10 <i class="glyphicon glyphicon-star"></i></strong></small>
								</div>	
							</td>		
					</tr>
					<tr>
						<td valign='top' > 
							<div style='border: 1px solid #DEDEDE; padding:5px; background:#FFFFFF; border-radius: 3px;'>
								<div style='border: 1px solid #DEDEDE; padding:3px; border-radius:2px;'>
									<img src='images/ZK/triumf.jpg' style='width:150px; height:100px;'/>
								</div>
							<small style='font-family:Verdana, Arial, Helvetica, sans-serif; color:#317EAC;'>Академия<br />
							<span style="color:#3B3B3B">Есильский район</br>
							п. Косщи, ул. Республик</br></span>
							<strong style='color:#F0C800;'>Рейтинг 9.25/10 <i class="glyphicon glyphicon-star"></i></strong></small>
								</div>	
							</td>		
					</tr>
					<tr>
						<td valign='top' > 
							<div style='border: 1px solid #DEDEDE; padding:5px; background:#FFFFFF; border-radius: 3px; max-width: 170px;'>
								<div style='border: 1px solid #DEDEDE; padding:3px; border-radius:2px;'>
									<img src='images/ZK/AltynTulip.jpg' style='width:150px; height:100px;'/>
								</div>
							<small style='font-family:Verdana, Arial, Helvetica, sans-serif; color:#317EAC;'>Триумфиальный<br />
							<span style="color:#3B3B3B">Алматинский район</br>
							Туран — Ханов Керея и Жанибека, 2</br></span>
							<strong style='color:#F0C800;'>Рейтинг 9.65/10 <i class="glyphicon glyphicon-star"></i></strong></small>
								</div>	
							</td>		
					</tr>
					
					</table>
				</div>
				
				<div style="width:100%; float:right;">
				<hr>
				<footer>
                	<p>&copy; 2016 - Агенство недвижимости</p>
           		</footer>
				</div>
				
</body>
</html>﻿