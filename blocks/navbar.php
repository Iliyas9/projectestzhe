﻿

<div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
			
                    </button>
<img src="images/1.png" style="max-height:50px;" />
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php">Главная</a></li>
						<li><a  href="messages.php" > Оставить заявку <span class="glyphicon glyphicon-pencil"></span></a></li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Продажа <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="selling_cottage.php" >Дома</a></li>
                                <li><a href="selling_apartment.php">Квартиры</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">Аренда <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="rent_cottage.php" >Дома</a></li>
                                <li><a href="rent_apartment.php">Квартиры</a></li>
                            </ul>
                        </li>
                        
						<li><a href="index.php"><span class="glyphicon glyphicon-tower"></span>&nbsp; Жилые комплексы</a></li>
                        
                    </ul>
                    
                </div>
            </div>
        </div>
	
	
		 <?php
			if (empty($_SESSION['email']) or empty($_SESSION['id']))
			{
				echo "<form class='navbar-form navbar-right' role='search' style='margin:0px; margin-top:-10px; font-style:italic'>
					  <div style='margin-right:130px; background:#F5F5F5; padding:10px; border:1px solid #E9E9E9; border-bottom-left-radius:7px; border-bottom-right-radius:7px;'>
						<img src='images/lock.png' width='14px' height='16px' style='margin-right:5px;' />
							<span class='register_span' style='margin-right:20px;' data-toggle='modal' data-target='.bs-example-modal-sm'>вход</span>
						<img src='images/Document-write-icon.png' width='14px' height='16px' style='margin-right:5px;' />
							<span class='register_span' data-toggle='modal' data-toggle='modal' data-target='#modal_register'>регистрация</span>
					  </div>
					</form>";
			}
			else
			{
			echo "<form class='navbar-form navbar-right' role='search' style='margin:0px; margin-top:-10px;'>
					  <div style='margin-right:130px; background:#F5F5F5; padding:10px; border:1px solid #E9E9E9; border-bottom-left-radius:7px; border-bottom-right-radius:7px;'>
			<span class='glyphicon glyphicon-user' style='margin-right:5px;' ></span><span style='margin-right:5px;'>".$_SESSION['name']."</span> &nbsp; | &nbsp;
			
			<span style='margin-right:5px;'><a href='exit.php'>выйти</a></span><img src='images/icon-exit.png' width='14' height='16' />
					  </div>
				  </form>";	  
			}
			?>
		
			
	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
		<div class="modal-content">
		  <div class="modal-header" style="background:#F5F5F5; border-top-right-radius:10px; border-top-left-radius:10px;">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
				<h4 class="modal-title" style="color:#555555; font-family:Verdana;">Вход</h4>
			  </div>
			  <div class="modal-body">
				<form class="form-horizontal" role="form" id="myForm_login"  action="testreg.php" method="post">
				<div style="padding-left:40px; padding-right:40px;">
				  <div class="form-group">
					  <input type="text" class="form-control input-sm" id="inputEmail" placeholder="Email" data-error="Некорректный email!" name="email" required>
					 
				  </div>
				  <div class="form-group">
					 <input type="password" data-minlength="6" class="form-control input-sm" id="inputPassword" placeholder="Password" name="password" required>
				  </div>
				  <div class="form-group">
				  <button type="submit" class="btn btn-primary input-sm" style="padding-top:4px;">войти</button>
				  </div>
				</div>
				</form>
			  </div>
		</div>
	  </div>
	</div>
	
	<div class="modal fade"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="modal_register">
	  <div class="modal-dialog" style="width:400px;">
		<div class="modal-content">
		  <div class="modal-header" style="background:#F5F5F5; border-top-right-radius:10px; border-top-left-radius:10px;">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" style="color:#555555; font-family:Verdana;">Регистрация</h4>
			  </div>
			  <div class="modal-body">
				<form class="form-horizontal" role="form" id="myForm_register"  action="save_user.php" method="post">
				<div style="padding-left:40px; padding-right:40px;">
				  <div class="form-group">
						<label for="inputEmail" class="control-label">Имя</label>
						 <input type="name" class="form-control" id="inputName" placeholder="Iliyas" name="name" required>			  
				  </div>				
				  <div class="form-group">
					<label for="inputEmail" class="control-label">Email</label>
					<input type="email" class="form-control" id="inputEmail" placeholder="iaripkul@gmail.com" data-error="Некорректный email!" name="email" required>
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group">
				  	 <label for="inputEmail" class="control-label">Пароль</label>
					 <input type="password" data-minlength="6" class="form-control" id="inputPassword1" placeholder="Пароль" name="password" required>
				  </div>
				  
				  <div class="form-group">
				  <button type="submit" class="btn btn-primary" style="padding-top:4px;">войти</button>
				  </div>
				</div>
				</form>
			  </div>
		</div>
	  </div>
	</div>
	
	